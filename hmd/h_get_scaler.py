from sklearn.preprocessing import MinMaxScaler, Normalizer, StandardScaler


def h_get_scaler(scaler: str = "MinMaxScaler") -> object:
    scalers = {
        "MinMaxScaler": MinMaxScaler,
        "Normalizer": Normalizer,
        'StandardScaler': StandardScaler
    }

    return scalers[scaler]()
