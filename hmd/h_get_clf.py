from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB
from sklearn.svm import SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


def h_get_clf(clf:str='', params:dict={}) -> object:
    classifiers = {
        "k-Nearest Neighbors": KNeighborsClassifier,
        "Logistic Regression": LogisticRegression,
        'Decision Tree': DecisionTreeClassifier,
        'Gaussian NB': GaussianNB,
        'Bernoulli NB': BernoulliNB,
        'Multinomial NB': MultinomialNB,
        'Support Vector Machine': SVC,
        'LDA': LinearDiscriminantAnalysis
    }

    return classifiers[clf](**params)
