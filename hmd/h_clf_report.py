import sys
import numpy as np
from .h_confusion_matrix import h_confusion_matrix


np.seterr(divide='ignore', invalid='ignore')
ESP = sys.float_info.epsilon


def h_clf_report(y_true, y_pred, digits=2):
    """
        return (
            accuracy_score,
            weighted_avg_precision,
            weighted_avg_precall,
            weighted_avg_f1-score
        )
    """
    cf_matrix = h_confusion_matrix(y_true, y_pred)    
    cm_diagonal = cf_matrix.diagonal()
    class_samples = np.sum(cf_matrix, axis=1)
    class_predicted = np.sum(cf_matrix, axis=0)
    total_samples = np.sum(cf_matrix)
    
    accuracy_score = np.trace(cf_matrix) / np.sum(cf_matrix)
    
    precision = np.nan_to_num(cm_diagonal / class_predicted)
    recall = np.nan_to_num(cm_diagonal / class_samples)
    f1 = np.nan_to_num(2 * (precision * recall) / (precision + recall))
    
    weighted_avg_precision = np.sum((precision + ESP) * class_samples) / total_samples
    weighted_avg_precall = np.sum((recall + ESP) * class_samples) / total_samples 
    weighted_avg_f1 = np.sum((f1 + ESP) * class_samples) / total_samples 
    
    return [
        round(accuracy_score, digits),
        round(weighted_avg_precision, digits),
        round(weighted_avg_precall, digits),
        round(weighted_avg_f1, digits)
    ]
