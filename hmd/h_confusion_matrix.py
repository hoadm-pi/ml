import numpy as np


def h_confusion_matrix(y_true, y_pred):
    n_classes = len(np.unique(y_true))
    confusion_matrix = np.zeros((n_classes, n_classes), dtype=np.int32)

    if len(y_true) != len(y_pred):
        raise ValueError("y_true and y_pred must be same length!")

    for i in range(y_true.shape[0]):
        confusion_matrix[y_true[i]][y_pred[i]] += 1

    return confusion_matrix
