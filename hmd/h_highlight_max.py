def h_highlight_max(s):
    is_max = (s == s.max())
    return ['color: red; font-weight: 700' if v else '' for v in is_max]
