# Homework - Machine Learning

Lưu trữ lại các bài tập về nhà môn ML. 

Một số lưu ý nhỏ:

* Thư viện [sklearn](https://scikit-learn.org/stable/) được sử dụng chính.
* Các hàm có bắt đầu bằng tiền tố 'h_' là hàm tự viết, mô phỏng lại các hàm của thư viện. Có thể có lỗi hoặc ngoại lệ --> cẩn thận khi sử dụng.
